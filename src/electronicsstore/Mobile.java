/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Jonnalagadda Jaswanth 
 */
public class Mobile  extends ElectronicStore
{
    private String model;
    private String processor;
    private String operatingSystem;
    private String body;
    private double battery;
    private double initilPrice;
    private List<String> accessories;
    /**
     * This is a no argument constructor for the class Mobile
     */
    public Mobile()
    {
        super();
        this.accessories = new ArrayList<String>();
    }
    /**
     * This is a constructor for the class Mobile
     * @param model it returns a String value
     * @param processor it returns a String value
     * @param operatingSystem it returns a String value
     * @param body it returns a String value
     * @param battery it returns a double value
     * @param initilPrice it returns a double value
     * @param storeName it returns a String value
     * @param address it returns a String value
     * @param contactNumer it returns a String value
     */
    public Mobile(String model, String processor, String operatingSystem, String body, double battery, double initilPrice, String storeName, String address, String contactNumer) 
    {
        super(storeName, address, contactNumer);
        this.model = model;
        this.processor = processor;
        this.operatingSystem = operatingSystem;
        this.body = body;
        this.battery = battery;
        this.initilPrice = initilPrice;
        this.accessories = new ArrayList();
    }

    /**
     * This method adds accessories to the mobile
     * @param items is a string value
     * @return it returns accessories
     */
    public List<String> addAccessories(String items)
    {
        for(String val : items.split(","))
        {
            accessories.add(val);
        }
         return accessories;
    }

    /**
     * This method get the model 
     * @return it returns a string value
     */
    public String getModel() {
        return model;
    }
    /**
     * This get returns the processor
     * @return it returns a string value 
     */
    public String getProcessor() {
        return processor;
    }
    /**
     * This method returns operating system
     * @return it returns a string value
     */
    public String getOperatingSystem() {
        return operatingSystem;
    }
    /**
     * This method returns the body
     * @return it returns a string value
     */
    public String getBody() {
        return body;
    }
    
    /**
     * This method returns the battery
     * @return it returns a double value
     */
    public double getBattery() {
        return battery;
    }
    
    /**
     * This method returns the initial price
     * @return it returns a integer value
     */
    public double getInitilPrice() {
        return initilPrice;
    }
    
    /**
     * This method returns the accessories
     * @return it returns a integer value
     */
    public List<String> getAccessories() {
        return accessories;
    }

   
   /**
    * This method prints the formatted output
    * @return it returns the string value
    */
    
    @Override
    public String toString() {
        return "Mobile{" + "model=" + model + ", processor=" + processor + ", operatingSystem=" + operatingSystem + ", body=" + body + ", battery=" + battery + ", initilPrice=" + initilPrice + ", accessories=" + accessories + '}';
    }

   
    
    
}
