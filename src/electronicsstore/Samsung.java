/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

/**
 *
 * @author Jonnalagadda Jaswanth 
 */
public class Samsung extends Mobile
{
    private final int STANDARD_BATTERY_LIFE = 10;
    
   /**
    * This is a constructor for the class samsung
    * @param model returns a string value
    * @param processor returns a string value
    * @param operatingSystem returns a string value
    * @param body returns a string value
    * @param battery returns a double value
    * @param initialPrice returns a double value
    * @param storeName returns a string value
    * @param address returns a string value
    * @param contactNumber returns a string value
    */
    public Samsung(String model,String processor, String operatingSystem, String body,double battery,double initialPrice,String storeName,String address,String contactNumber)
    {
        super(model,processor,operatingSystem,body,battery,initialPrice,storeName,address,contactNumber);
    }
    /**
     * This is a no argument constructor for the class Samsung
     */
    public Samsung()
    {
        super();
    }
    double batterylife;
    /**
     * This method returns the total cost
     * @return it returns an integer value
     */
    public double caluclateCost()
    {
     //   Mobile m = new Mobile();
        double price = getInitilPrice();
        for(String acc : getAccessories())
        {
            if(acc.contains("Bluetooth headsets"))
              price += 79;
            if(acc.contains("USB cable"))
              price +=17;
            if(acc.contains("Wired headsets with mic"))
                price +=39;
            if(acc.contains("Extended 1 year warranty"))
                price +=89;
            if(acc.contains("Charging Adapter"))
                price +=29;
        }
            return price;
    }
    /**
     * This method returns the battery life
     * @param noOfHoursin4G is an integer value
     * @return returns double value
     */
   public double calucalteBatteryLife(int noOfHoursin4G)
        {
           
            batterylife = (STANDARD_BATTERY_LIFE / noOfHoursin4G );
            return (double)batterylife;
        }
    /**
     * This method returns the formatted output
     * @return it returns a string value
     */
    @Override
    public String toString() {
       return "Samsung Phone Details:"+"\nModel:"+getModel()+"\nProcessor:"+getProcessor()+"\nBattery:"+getBattery()+"mAh"+"\nBody:"+getBody()+"\nOperating System:"+getOperatingSystem()+"\nInitial Price:$"+getInitilPrice()+"0"+"\nBattery Life:"+batterylife+"0"+"hours"+"\nTotal Price:$"+caluclateCost()+"0";
    
    }
    
}
