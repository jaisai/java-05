/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

/**
 *
 * @author Jonnalagadda Jaswanth
 */
public class Telivision extends ElectronicStore
{
    private String model;
    private String type;
    private double screensize;
    private double weight;
    private double cost;
    
 /*   public Telivision(String model,String type,double screensize,double weight,String storeName,String address,String contactNumber,double cost)
    {
        super(model,type,screensize,weight,storeName,address,contactNumber,cost);
    }*/
    /**
     * This is the constructor for the class Telivison
     * @param model is a string value
     * @param type is a string value
     * @param screensize is a double value
     * @param weight is a double value
     * @param storeName is a string value
     * @param address is a string value
     * @param contactNumer is a string value
     * @param cost is a double value
     */
        public Telivision(String model, String type, double screensize, double weight, String storeName, 
                String address, String contactNumer,double cost) 
        {
           super(storeName,address,contactNumer);
           this.model = model;
           this.type = type;
           this.screensize = screensize;
           this.weight = weight;
           this.cost = cost;
        }   
        /**
         * This method returns the model
         * @return it returns a string value
         */
    public String getModel() {
        return model;
    }
    /**
         * This method returns the cost
         * @return it returns a double value
         */
    public double getCost() {
        return cost;
    }
    /**
         * This method returns the type
         * @return it returns a string value
         */
    public String getType() {
        return type;
    }
    /**
         * This method returns the screen size
         * @return it returns a double value
         */
    public double getScreensize() {
        return screensize;
    }
    /**
         * This method returns the weight
         * @return it returns a double value
         */
    public double getWeight() {
        return weight;
    }
    double tCost;
    /**
     * This method is used to calculate total cost
     * @param is3D is a boolean value
     * @param isAutopoweroff is a boolean value
     * @param isGameMode is a boolean value
     * @return returns the total cost of television 
     */
    public double calculateCost(boolean is3D,boolean isAutopoweroff,boolean isGameMode){
      tCost=cost;
      if(is3D == true){
          tCost+=100;
    }
     if(isAutopoweroff==true){
        tCost+=25;
     }
     if(isGameMode==true){
         tCost+=25;
     }
     return tCost;
     
  }
    /**
     * This method returns the formatted string
     * @return returns a string value
     */
     @Override
    public String toString() {
        return "\nmodel=" + model + "\ntype=" + type + "\nscreensize=" + screensize+" inches" + "\nweight=" + weight+" kgs" + "\ncost=" +tCost;
    }
    

    
    
        
}
