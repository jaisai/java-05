/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

/**
 *
 * @author Jonnalagadda Jaswanth 
 */
public class ElectronicStore 
{
    private String storeName;
    private String address;
    private String contactNumer;
    /**
     * This is a no argument constructor
     */
    public ElectronicStore() 
    {
        
    }
    /**
     * This is the constructor for the class Electronic Store
     * @param storeName is a String value
     * @param address is a String value
     * @param contactNumber is a String value
     */
    public ElectronicStore(String storeName, String address, String contactNumber) {
        this.storeName = storeName;
        this.address = address;
        this.contactNumer = contactNumber;
    }
    /**
     * It gets the StoreName
     * @return it returns string value
     */
    public String getStoreName() {
        return storeName;
    }
    /**
     * This method returns the address
     * @return it returns a string
     */
    public String getAddress() {
        return address;
    }
    /**
     * This method gets contact number
     * @return it returns a string value
     */
    public String getContactNumer() {
        return contactNumer;
    }
    /**
     * This methods print the formatted string
     * @return it returns a string value
     */
    @Override
    public String toString() {
        return "Electronic Store Details:"+"\n" + "Store Name:" + storeName +"\n"+ "Store Address:" + address +"\n"+ "Store Contact Numer:" + contactNumer ;
    }
    
    
    
    
    
}
