/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Jaswanth Jonnalagadda
 */
public class ElectronicStoreDriver {

    public static void main(String args[]) throws FileNotFoundException {
        Scanner scannerobj = new Scanner(new File("electronics.txt"));
        ElectronicStore ESobj = new ElectronicStore();
        ESobj.toString();
        

        String storeName = scannerobj.nextLine();
        String address = scannerobj.nextLine();
        String contactNumber = scannerobj.nextLine();
        ElectronicStore ESobj1 = new ElectronicStore(storeName, address, contactNumber);
        System.out.println(ESobj1.toString());
        System.out.println("*****************************************************************");
        while (scannerobj.hasNext()) {
            switch (scannerobj.nextLine()) {

                case "Mobile":

                   System.out.println();
                    String model = scannerobj.nextLine();

                    String processor = scannerobj.nextLine();
                    String operatingsystem = scannerobj.nextLine();
                    String body = scannerobj.nextLine();

                    double battery = Double.parseDouble(scannerobj.nextLine());
                    double intialPrice = Double.parseDouble(scannerobj.nextLine());
                   
                    String accesories = scannerobj.nextLine();
                    int hours = Integer.parseInt(scannerobj.nextLine());
                   
                  
                    if (model.equals("iPhone")) {
                        Apple m1 = new Apple(model, processor, operatingsystem, body, battery, intialPrice, storeName, address, contactNumber);
                        m1.addAccessories(accesories);
                        m1.calucalteBatteryLife(hours);
                        System.out.println(m1.toString());
                        System.out.println("*****************************************************************\n");
                    }
                    if (model.equals("Samsung")) {
                        Samsung s1 = new Samsung(model, processor, operatingsystem, body, battery, intialPrice, storeName, address, contactNumber);
                        s1.addAccessories(accesories);
                        s1.calucalteBatteryLife(hours);
                   
                        System.out.println(s1.toString());
                        System.out.println("*****************************************************************\n");
                    }
                    break;

                case "Television":
                    String tvModel = scannerobj.nextLine();
                    String tvType = scannerobj.nextLine();
                    double tvSize = Double.parseDouble(scannerobj.nextLine());
                    double tvWeight = Double.parseDouble(scannerobj.nextLine());
                    double tvCost = Double.parseDouble(scannerobj.nextLine());
                    boolean tv3D = Boolean.parseBoolean(scannerobj.nextLine());
                    boolean tvAutoPowerOff = Boolean.parseBoolean(scannerobj.nextLine());
                    boolean tvGameMode = Boolean.parseBoolean(scannerobj.nextLine());
                                    
                    if (tvModel.equals("Samsung J5205")) {
                       
                        Telivision t1 = new SmartTelivision(tv3D,tvAutoPowerOff,tvGameMode,tvModel, tvType, tvSize, tvWeight, storeName, address, contactNumber, tvCost);
                        t1.calculateCost(tv3D, tvAutoPowerOff, tvGameMode);
                        
                        System.out.println(t1.toString());
                        System.out.println("*****************************************************************\n");
                    }
                    if (tvModel.equals("LG 22LF430A")) {
                        Telivision t1 = new SmartTelivision(tv3D,tvAutoPowerOff,tvGameMode,tvModel, tvType, tvSize, tvWeight, storeName, address, contactNumber, tvCost);                      
                        
                        System.out.println(t1.toString());
                        System.out.println("*****************************************************************\n");
                    
                    }

                        
                    }

            }
            
        }
       

    }
