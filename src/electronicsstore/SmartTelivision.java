/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

/**
 *
 * @author Jaswanth Jonnalagadda
 */
public class SmartTelivision extends Telivision
{
    private boolean is3D;
    private boolean isAutoPowerOff;
    private boolean isGameMode;
    /**
     * This is a no argument constructor for the class Smart Telivision
     * @param is3D is a boolean value
     * @param isAutoPowerOff is a boolean value
     * @param isGameMode is a boolean value
     * @param model is a string value
     * @param type is a string value
     * @param screensize is a double value
     * @param weight is a double value
     * @param storeName is a string value
     * @param address is a string value
     * @param contactNumer is a string value
     * @param cost is a double value
     */
    public SmartTelivision(boolean is3D, boolean isAutoPowerOff, boolean isGameMode, String model, String type, 
            double screensize, double weight, String storeName, String address, String contactNumer,double cost)
    {
        super(model, type, screensize, weight,storeName, address, contactNumer,cost);
        this.is3D = is3D;
        this.isAutoPowerOff = isAutoPowerOff;
        this.isGameMode = isGameMode;
    }
    
    
    /**
     * This method returns the formatted string
     * @return it returns a string value
     */
      @Override
    public String toString() {
       // System.out.println(is3D+"   "+isAutoPoweroff+"    "+isGameMode);
        return "Television Details:"+"\nModel:"+getModel()+
"\nType:"+getType()+"\nScreen size:"+getScreensize()+" inches"+"\nWeight:"+getWeight()+" kgs"+"\nis3D:"+is3D+"\nisAutoPowerOff:"+isAutoPowerOff+"\nisGameMode:"+isGameMode+"\nTotal Cost:$"+calculateCost(is3D, isAutoPowerOff, isGameMode);

    
    
}
    
}
