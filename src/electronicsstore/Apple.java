/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package electronicsstore;

import java.util.ArrayList;

/**
 *
 * @author Jaswanth Jonnalagadda
 */
public class Apple extends Mobile 
{
    private final int  STANDARD_BATTERY_LIFE = 12;
        /**
         * This constructor for the class Apple
         * @param model is a String value
         * @param Processor is a string value
         * @param operatingSystem is a string value
         * @param body is a string value
         * @param battery is a double value
         * @param initialPrice is a double value
         * @param storeName is a string value
         * @param address is a string value
         * @param contactNumber  is a string value
         */
        public Apple(String model,String Processor, String operatingSystem, String body, double battery, 
                double initialPrice, String storeName, String address,String contactNumber)
       {
           super(model,Processor,operatingSystem,body,battery,initialPrice,storeName,address,contactNumber);
       }
        /**
         * This is a no argument constructor
         */
         public Apple() 
        {
            super();
        }
         
         double batterylife; 
         
       /**
        * This method calculates cost
        * @return it return price
        */
         public double caluclateCost() 
        {
           // Mobile m = new Mobile();
            double price = getInitilPrice();
            for(String acc : getAccessories())
            {
                if(acc.contains("Bluetooth headsets"))
                {
                    price += 99;
                }
                if(acc.contains("USB cable"))
                {
                    price += 20;
                }
                if(acc.contains("Wired headsets with mic"))
                {
                    price += 49;
                }
                if(acc.contains("Extended 1 year warranty"))
                {
                    price += 99;
                }
                if(acc.contains("Charging Adapter"))
                {
                    price += 39;
                }
                
            }
            return price;
        }
       /**
        * This method returns battery life
        * @param noOfHoursin4G is integer value
        * @return it returns batterylife
        */
        public double calucalteBatteryLife(int noOfHoursin4G)
        {
           
            batterylife = ((double)STANDARD_BATTERY_LIFE / noOfHoursin4G );
            return batterylife;
        }
        /**
         * This method prints the formatted output
         * @return it returns string value
         */
    @Override
    public String toString() {
        String initprice = String.format("%.2f",getInitilPrice() );
        String batLife = String.format("%.2f",batterylife );
        String totPrice= String.format("%.2f", caluclateCost());
        return "iPhone Details:"+"\nModel:"+getModel()+"\nProcessor:"+getProcessor()+"\nBattery:"+getBattery()+"mAh"+"\nBody:"+getBody()+"\nOperating System:"+getOperatingSystem()+"\nInitial Price:$"+getInitilPrice()+"0"+"\nBattery Life:"+batLife+"hours"+"\nTotal Price:$"+caluclateCost()+"0";
    }
}
